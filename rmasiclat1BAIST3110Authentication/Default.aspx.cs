﻿using System;
using System.Web.Security;
using System.Security.Principal;
using System.Web;

public partial class _Default : System.Web.UI.Page {
	protected void Page_Load(object sender, EventArgs e) {
		CustomPrincipal cp = HttpContext.Current.User as CustomPrincipal;

		Response.Write("Authenticated Identity is: " + cp.Identity.Name);
		Response.Write("<br />");
		if (cp.IsInRole("Admin")) {
			Response.Write(cp.Identity.Name + " is in the Admin role<br />");
		}
		else
		{
			Response.Write(cp.Identity.Name + "is not an Admin<br />");
		}
		Welcome.Text = "Hello, " + Context.User.Identity.Name;
	}

	protected void Signout_Click(object sender, EventArgs e) {
		FormsAuthentication.SignOut();
		Session.Clear();
		Session.Abandon();
		Response.Redirect("Logon.aspx");
	}
}