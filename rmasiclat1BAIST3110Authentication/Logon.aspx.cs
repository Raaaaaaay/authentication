﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;

public partial class Logon : Page {
	protected void Page_Load(object sender, EventArgs e)
	{

	}

	protected void Logon_Click(object sender, EventArgs e)
	{
		Controller uc = new Controller();
		bool isAuthenticated = IsAuthenticated(UserEmail.Text, UserPass.Text);

		if (isAuthenticated)
		{
			string roles = uc.GetRoles(UserEmail.Text);
			FormsAuthenticationTicket authTicket = new FormsAuthenticationTicket(1,
				UserEmail.Text,
				DateTime.Now,
				DateTime.Now.AddMinutes(60),
				false,
				roles);
			//Encrypt Ticket
			string encryptedTicket = FormsAuthentication.Encrypt(authTicket);
			//Create a cookie and add the encrypted ticket to the cookie as data
			HttpCookie authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedTicket);

			//Add cookie to the cookies collection returned to the user's browser.
			Response.Cookies.Add(authCookie);
			Response.Redirect(FormsAuthentication.GetRedirectUrl(UserEmail.Text, false));
		} else {
			Msg.Text = "Login failed. Please check your user name and password and try again.";
		}
		//Controller uc = new Controller();

		////User aUser = new User(UserEmail.Text, UserPass.Text);
		//string username = UserEmail.Text;
		//string password = UserPass.Text;
		//bool isPersistent = false;

		//if (uc.ValidateUser(username, password)) {
		//	////Response.Redirect("~/Default.aspx");
		//	//FormsAuthentication.RedirectFromLoginPage
		//	//	(UserEmail.Text, Persist.Checked);
		//	string userData = "ApplicationSpecific data for this user.";

		//	FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
		//		username,
		//		DateTime.Now,
		//		DateTime.Now.AddMinutes(30),
		//		isPersistent,
		//		userData,
		//		FormsAuthentication.FormsCookiePath);

		//	//Encrypt the ticket
		//	string encTicket = FormsAuthentication.Encrypt(ticket);

		//	//Create the cookie.
		//	Response.Cookies.Add(new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));

		//	//Redirect back to original URL.
		//	Response.Redirect(FormsAuthentication.GetRedirectUrl(username, isPersistent));

		//} else {
		//	Msg.Text = "Login failed. Please check your user name and password and try again.";
		//}
	}

	private bool IsAuthenticated(string username, string password)
	{
		Controller uc = new Controller();
		return uc.ValidateUser(username, password);
	}

	protected void Register_OnClick(object sender, EventArgs e)
	{
		Response.Redirect("Register.aspx");
	}
}
