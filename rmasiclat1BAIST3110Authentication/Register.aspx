﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Register.aspx.cs" Inherits="Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <h3>Register Page</h3>
        <asp:Table ID="Table1" runat="server">
            <asp:TableRow>
                <asp:TableCell>Email Address:</asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="UserEmail" runat="server"/>
                    <asp:RequiredFieldValidator 
                        ID="UserEmailRequiredFieldValidator" 
                        runat="server" 
                        ErrorMessage="Cannot be empty."
                        ControlToValidate="UserEmail"
                        Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>Password:</asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="UserPass" runat="server" TextMode="Password"/>
                    <asp:RequiredFieldValidator 
                        ID="UserPassRequiredFieldValidator" 
                        runat="server" 
                        ErrorMessage="Cannot be empty."
                        ControlToValidate="UserPass"
                        Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:Button ID="Submit" runat="server" Text="Register" OnClick="Register_Click"/>
        <p>
            <asp:Label ID="Msg" ForeColor="red" runat="server"></asp:Label>
        </p>
    </form>
    <a href="Logon.aspx">Log in</a>
</body>
</html>