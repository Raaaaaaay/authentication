﻿public class User {
	private string useremail;
	private string password;
	private string salt;
	private string role;

	public User() {
	}

	public User(string useremail, string password, string salt, string role) {
		this.Useremail = useremail;
		this.Password = password;
		this.Salt = salt;
		this.Role = role;

	}

	public string Useremail { get => useremail; set => useremail = value; }
	public string Password { get => password; set => password = value; }
	public string Salt { get => salt; set => salt = value; }
	public string Role { get => role; set => role = value; }
}