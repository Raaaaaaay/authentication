﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;

public class SecurityManager {
	public byte[] CreateSalt()
	{
		byte[] salt = new byte[8];
		using (var rng = RandomNumberGenerator.Create())
		{
			rng.GetBytes(salt);
		}

		return salt;
	}

	public string CreatePasswordHash(string pwd, byte[] salt)
	{
		Rfc2898DeriveBytes pbkdf2 = new Rfc2898DeriveBytes(pwd,salt,1000);
		return Encoding.ASCII.GetString(pbkdf2.GetBytes(20));
	}

	public bool ValidateUser(string email, string password)
	{
		SecurityManager sm = new SecurityManager();
		User check = sm.GetUser(email);
		if (check.Password == null)
		{
			return false;
		}
		else
		{
			string pass = sm.CreatePasswordHash(password, Encoding.ASCII.GetBytes(check.Salt));
			if (pass.Equals(check.Password)) {
				return true;
			} else {
				return false;
			}
		}
	}

	public bool AddUser(User newUser) {

		SqlConnection db = new SqlConnection {
			ConnectionString = ConfigurationManager.ConnectionStrings["db"].ConnectionString
		};
		db.Open();

		SqlCommand AddUserCommand = new SqlCommand {
			Connection = db,
			CommandType = CommandType.StoredProcedure,
			CommandText = "AddUser"
		};

		SqlParameter AddUserParameter = new SqlParameter {
			ParameterName = "@UserEmail",
			SqlDbType = SqlDbType.VarChar,
			Direction = ParameterDirection.Input,
			SqlValue = newUser.Useremail
		};

		AddUserCommand.Parameters.Add(AddUserParameter);

		AddUserParameter = new SqlParameter {
			ParameterName = "@Password",
			SqlDbType = SqlDbType.VarChar,
			Direction = ParameterDirection.Input,
			SqlValue = newUser.Password
		};

		AddUserCommand.Parameters.Add(AddUserParameter);

		AddUserParameter = new SqlParameter {
			ParameterName = "@Salt",
			SqlDbType = SqlDbType.VarChar,
			Direction = ParameterDirection.Input,
			SqlValue = newUser.Salt
		};

		AddUserCommand.Parameters.Add(AddUserParameter);

		AddUserParameter = new SqlParameter {
			ParameterName = "@Roles",
			SqlDbType = SqlDbType.VarChar,
			Direction = ParameterDirection.Input,
			SqlValue = newUser.Role
		};

		AddUserCommand.Parameters.Add(AddUserParameter);

		try {
			AddUserCommand.ExecuteNonQuery();
			return true;
		} catch {
			return false;
		} finally {
			db.Close();
		}
	}

	public User GetUser(String email) {
		SqlConnection db = new SqlConnection {
			ConnectionString = ConfigurationManager.ConnectionStrings["db"].ConnectionString
		};
		db.Open();

		SqlCommand GetUserCommand = new SqlCommand {
			Connection = db,
			CommandType = CommandType.StoredProcedure,
			CommandText = "GetUser"
		};

		SqlParameter GetUserParameter = new SqlParameter {
			ParameterName = "@UserEmail",
			SqlDbType = SqlDbType.VarChar,
			Direction = ParameterDirection.Input,
			SqlValue = email
		};

		GetUserCommand.Parameters.Add(GetUserParameter);

		SqlDataReader dr = GetUserCommand.ExecuteReader();
		if (dr.HasRows) {
			dr.Read();
			return new User(dr["UserEmail"].ToString(), dr["Password"].ToString(), dr["Salt"].ToString(), dr["Roles"].ToString());
		} else {
			return new User();
		}
	}

	public bool ValidateUser(User aUser) {
		User check = GetUser(aUser.Useremail);
		if (check.Useremail == null || check.Password == null || check.Password != aUser.Password) {
			return false;
		} else {
			return true;
		}
	}

	public string GetRoles(string username)
	{
		SqlConnection db = new SqlConnection {
			ConnectionString = ConfigurationManager.ConnectionStrings["db"].ConnectionString
		};
		db.Open();

		SqlCommand GetUserCommand = new SqlCommand {
			Connection = db,
			CommandType = CommandType.StoredProcedure,
			CommandText = "GetRoles"
		};

		SqlParameter GetUserParameter = new SqlParameter {
			ParameterName = "@UserEmail",
			SqlDbType = SqlDbType.VarChar,
			Direction = ParameterDirection.Input,
			SqlValue = username
		};

		GetUserCommand.Parameters.Add(GetUserParameter);

		SqlDataReader dr = GetUserCommand.ExecuteReader();
		if (dr.HasRows) {
			dr.Read();
			return dr["Roles"].ToString();
		} else {
			return null;
		}
	}
}