﻿using System;

public class Controller {
	public bool AddUser(User user) {
		return new SecurityManager().AddUser(user);
	}

	public bool ValidateUser(User aUser) {
		return new SecurityManager().ValidateUser(aUser);
	}

	public bool ValidateUser(String user, String password)
	{
		return new SecurityManager().ValidateUser(user,password);
	}

	public string GetRoles(string username) {
		return new SecurityManager().GetRoles(username);
	}
}