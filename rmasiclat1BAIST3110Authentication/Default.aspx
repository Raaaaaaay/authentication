﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Forms Authentication - Default Page</title>
</head>
<body>
    <h3>Using Forms Authentication</h3>
    <asp:Label ID="Welcome" runat="server"></asp:Label>
    <form id="form1" runat="server">
        <asp:Button ID="Submit" runat="server" Text="Sign Out" OnClick="Signout_Click"/>
    </form>
    <a href="Register.aspx">Register</a>
</body>
</html>
