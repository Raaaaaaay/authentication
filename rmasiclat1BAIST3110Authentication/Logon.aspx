﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Logon.aspx.cs" Inherits="Logon" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Forms Authentication - Login</title>
</head>
<body>
    <form id="form1" runat="server">
        <h3>Logon Page</h3>
        <asp:Table ID="Table1" runat="server">
            <asp:TableRow>
                <asp:TableCell>Email Address:</asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="UserEmail" runat="server"/>
                    <asp:RequiredFieldValidator 
                        ID="UserEmailRequiredFieldValidator" 
                        runat="server" 
                        ErrorMessage="Cannot be empty."
                        ControlToValidate="UserEmail"
                        Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>Password:</asp:TableCell>
                <asp:TableCell>
                    <asp:TextBox ID="UserPass" runat="server" TextMode="Password"/>
                    <asp:RequiredFieldValidator 
                        ID="UserPassRequiredFieldValidator" 
                        runat="server" 
                        ErrorMessage="Cannot be empty."
                        ControlToValidate="UserPass"
                        Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>Remember Me?</asp:TableCell>
                <asp:TableCell><asp:CheckBox ID="Persist" runat="server" /></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Right"><asp:Button ID="Submit" runat="server" Text="Log On" OnClick="Logon_Click"/></asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <p>
            <asp:Label ID="Msg" ForeColor="red" runat="server"></asp:Label>
        </p>
    </form>
</body>
</html>

