﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Register : Page {
	protected void Page_Load(object sender, EventArgs e)
	{
		CustomPrincipal cp = HttpContext.Current.User as CustomPrincipal;
		if (cp == null) {
			Response.Redirect("Default.aspx",true);
		}
	}

	protected void Register_Click(object sender, EventArgs e)
	{
		CustomPrincipal cp = HttpContext.Current.User as CustomPrincipal;
		if (!cp.IsInRole("Admin")) {
			Response.Write("You must be an administrator to register users");
		}
		else
		{
			SecurityManager sm = new SecurityManager();
			Controller uc = new Controller();
			string salt = Encoding.ASCII.GetString(sm.CreateSalt());
			string saltedPassword = sm.CreatePasswordHash(UserPass.Text, Encoding.ASCII.GetBytes(salt));
			if (uc.AddUser(new User(UserEmail.Text, saltedPassword, salt, "NotAdmin"))) {
				Msg.Text = "User is Registered!";
			} else {
				Msg.Text = "Register failed";
			}
		}

	}
}